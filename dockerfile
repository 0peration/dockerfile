FROM ubuntu

LABEL MAINTAINER="André Ribeiro <andreribeiroweb2009@gmail.com>"
LABEL APP_VERSION="1.0.0"

ENV NPM_VERSION=8 ENVIRONMENT=PROD
RUN apt-get update && apt-get install -y git nano npm

WORKDIR /usr/share/myapp

RUN npm build

COPY requirements.txt requirements.txt

ADD ./files.tar.gz ./

RUN useradd andre

USER andre

EXPOSE 8081

VOLUME [ "/data" ]

ENTRYPOINT [ "ping" ]

CMD [ "localhost" ]